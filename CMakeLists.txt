project(ffmpeg_related CXX)
cmake_minimum_required(VERSION 3.5)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

option(DEBUG "Switch for debug" ON)
if (DEBUG)
    # Debug mode
    ADD_DEFINITIONS(-DFF_DEBUG)
    set(CMAKE_BUILD_TYPE Debug) 
    message(STATUS "Debug mode ON" )

else(DEBUG)
    # Release mode
    #set(CMAKE_BUILD_TYPE Release)
    set(CMAKE_BUILD_TYPE RelWithDebInfo)
    set(CMAKE_C_FLAGS "-O4")
    message(STATUS "Debug mode OFF")

endif (DEBUG)

# Point CMake at any custom modules we may ship
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake")

# Enable assertions for RelWithDebInfo.
if(CMAKE_C_FLAGS_RELWITHDEBINFO MATCHES DNDEBUG)
    string(REPLACE "-DNDEBUG" "" CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO}")
endif()

set(INSTALL_PATH ${CMAKE_BINARY_DIR}/usr)
set(LIBRARY_OUTPUT_PATH ${INSTALL_PATH}/lib)
set(EXECUTABLE_OUTPUT_PATH ${INSTALL_PATH}/bin)

MARK_AS_ADVANCED(LIBRARY_OUTPUT_PATH EXECUTABLE_OUTPUT_PATH)

add_subdirectory(capture_save)
add_subdirectory(rtp_recv)
